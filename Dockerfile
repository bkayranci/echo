FROM python:3.9-alpine

RUN adduser -s /sbin/nologin -D turkalp
USER turkalp
WORKDIR /home/turkalp/apps

COPY requirements.txt .
ENV PATH=${PATH}:/home/turkalp/.local/bin
RUN pip install --user -r requirements.txt

COPY ./src/echo ./echo

EXPOSE 8083
ENTRYPOINT ["gunicorn", "echo:app", "--bind=0.0.0.0:8083", "--access-logfile=/dev/stdout", "--log-level=debug", "--threads=2", "--access-logformat", "{\"remote_ip\":\"%(h)s\",\"request_id\":\"%({X-Request-Id}i)s\",\"response_code\":\"%(s)s\",\"request_method\":\"%(m)s\",\"request_path\":\"%(U)s\",\"request_querystring\":\"%(q)s\",\"request_timetaken\":\"%(D)s\",\"response_length\":\"%(B)s\"}"]

