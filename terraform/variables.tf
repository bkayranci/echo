variable "do_token" {
  type = string
}

variable "k8s_version" {
  default = "1.20.7-do.0"
}

variable "k8s_region" {
  default = "nyc1"
}

variable "gitlab_access_token" {
  type = string
}


variable "echo_version" {
  type = string
  default = "latest"
}