provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_kubernetes_cluster" "echo-k8s" {
  name   = "echo-k8s"
  region = var.k8s_region
  version = var.k8s_version

  node_pool {
    name       = "autoscale-worker-pool"
    size       = "s-2vcpu-2gb"
    auto_scale = true
    min_nodes  = 1
    max_nodes  = 3
  }
}

provider "gitlab" {
  token = var.gitlab_access_token
}

resource "gitlab_project_variable" "echo_version" {
  project   = data.gitlab_project.echo.id
  key       = "echo_version"
  value     = var.echo_version
  protected = false
}

