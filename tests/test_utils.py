import os
import tempfile

import pytest

from echo import app

@pytest.fixture
def client():
    return app.test_client()

@pytest.fixture
def messages():
    return [
        ('<i>hello', '&lt;i&gt;hello')
    ]


def test_echo_function(client):
    """Test the echo function"""

    rv = client.get('/')
    assert b'Hello Migros, this is @Turkalp' in rv.data
    assert 200 == rv.status_code

def test_echo_function_with_parameter(client, messages):
    """Test the echo function"""

    for actual, expected in messages:
        rv = client.get(f'/?message={actual}')
        assert expected.encode() == rv.data
        assert 200 == rv.status_code