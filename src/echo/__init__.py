import logging
import os
import sys
from flask import Flask, request
from .extensions import scheduler
from .utils import echo_message, load_cpu

app = Flask(__name__)
scheduler.init_app(app)
@scheduler.task('interval', id='echo_message_job', seconds=60)
def scheduled_echo_message():
    print(echo_message())

scheduler.start()

@app.route('/')
def homepage():
    message = request.args.get('message')
    return echo_message(message)

@app.route('/load')
def load():
    load_cpu()
    return 'OK'


