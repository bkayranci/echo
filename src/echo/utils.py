from typing import Optional
from html import escape
import math

def echo_message(message: Optional[str] = None) -> str:
    """Return a message.

    Args:
        message: a string message.

    Returns:
        A string message.
    """

    return f'Hello Migros, this is @Turkalp' if message is None else escape(str(message))


def load_cpu(iter_size=1000000):
    x = 0.0001
    for _ in range(iter_size):
        x += math.sqrt(x)
    return True
