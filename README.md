# ECHO APP

The echo app returns a message response.


## Endpoints

- /

It returns a default message if you do not provide a message in querystring.

#### Query Params
- message: string



## Development

### Requirements
- Flask
- Gunicorn

### Installation
```terminal
pip install -r requirements.txt
```

### Run
```terminal
gunicorn src.echo:app --bind=127.0.0.1:8083
```


## Author & License
echo app is written and maintained by [Türkalp Burak Kayrancıoğlu](https://github.com/bkayranci/). It is licensed under an GPL v3 license (see LICENSE file).
